const mongoose = require('mongoose');
const crypto = require('crypto');

const User = new mongoose.Schema({
    _id: { type:String },
    name: {
        type: String,
        trim: true,
        required: [true, 'Le nom est obligatoire']
    },
    firstName: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'L\'email est obligatoire'],
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        trim: true,
    },
    data: {
        type: Object
    }
});

module.exports = mongoose.model('User', User);