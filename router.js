const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// eslint-disable-next-line new-cap
const router = express.Router();

const service = require('./service/userService');
const authService = require('./service/authService');

const swaggerOptions = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'User Rest API',
      version: '1.0.0',
      description: "Simple User management API.",
      contact: {
        name: "Les ambassadeurs"
      },
      servers: ["http://localhost:8000"]
    },
  },
  apis: ['router.js'], // files containing annotations as above
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
router.use('/api/v1/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/**
 * @openapi
 * /api/v1/users:
 *   get:
 *     description: Returns all users in database.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: email
 *         type: string
 *         description: If email (users?email=johndoe@gmail.com) parameters then you search only the User with this address email.
 *     responses:
 *       200:
 *         description: Returns (JSON) all users in database.
 *       404:
 *         description: Not Users in database.
 */
router.get('/api/v1/users', service.getUsers);

router.use('/', (req, res, next) => {
  if (req.url == '/') {
    res.redirect('/api/v1/users');
  } else {
    next();
  }
});

/**
 * @openapi
 * paths:
 *   /api/v1/user/{id}:
 *     get:
 *       description: Get user by his id.
 *       produces:
 *         - application/json
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           requires: true
 *           description: User id
 *       responses:
 *         200:
 *           description: Return (JSON) an user by his id.
 *         404:
 *           description: User Not Found.
 */
router.get('/api/v1/user/:id', service.getById);

/**
 * @openapi
 * /api/v1/login:
 *   post:
 *     description: Use to get auth token
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - email
 *               - password
 *             properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *     responses:
 *       200:
 *         description: Token
 *       404:
 *         description: User not found
 *       500:
 *         description: Internal Error
 */
router.post('/api/v1/login', authService.login);

/**
 * @openapi
 * paths:
 *   /api/v1/user/{id}:
 *     delete:
 *       description: Delete user by his id.
 *       produces:
 *         - application/json
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           requires: true
 *           description: User id
 *       responses:
 *         200:
 *           description: Return (JSON) an user by his id.
 *         404:
 *           description: User Not Found.
 */
router.delete('/api/v1/user/:id', service.delete);

/**
 * @openapi
 * /api/v1/user:
 *   post:
 *     description: Add User in database.
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: userName and email are required. email must to be unique in database.
 *             type: object
 *             required:
 *               - userName
 *               - email
 *             properties:
 *               userName:
 *                 type: string
 *               firstName:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *               data:
 *                 type: object
 *     responses:
 *       200:
 *         description: Return (JSON) an user by his id.
 *       501:
 *         description: Error message.
 */
router.post('/api/v1/user', service.add);

/**
 * @openapi
 * /api/v1/user:
 *   put:
 *     description: Add User in database.
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: userName and email are required. email must to be unique in database.
 *             type: object
 *             required:
 *               - userName
 *               - email
 *             properties:
 *               userName:
 *                 type: string
 *               firstName:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *               data:
 *                 type: object
 *     responses:
 *       200:
 *         description: Return (JSON) an user by his id.
 *       501:
 *         description: Error message.
 */
router.put('/api/v1/user', service.update);

router.use((req, res) => {
  res.status(404).send('Page non trouvée');
});

module.exports = router;