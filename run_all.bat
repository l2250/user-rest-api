if not "%1" == "" goto :%1

start "mailing" "%~dpfx0" mailing
start "storage" "%~dpfx0" storage
start "user" "%~dpfx0" user
goto :eof

:mailing
start "" http://localhost:4444/api-docs & cd mailing-rest-api && npm run start
echo mailing
pause
exit

:storage
start "" http://localhost:8001/api/v1/api-docs & cd storage-rest-api && npm start 
echo storage
pause
exit

:user
start "" http://localhost:8000/api/v1/users/api-docs/ & cd ../user_service_rest && docker compose up
echo user
pause
exit