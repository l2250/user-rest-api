const User = require('../models/user');
const mongoose = require('mongoose');

exports.getById = async (req,res) => {
    const id = req.params.id;
    try{
        let user = await User.findById(id);
        if(user){
            return res.status(200).json(user);
        }
        return res.status(404).json('User not found');
    } catch (err){
        return res.status(501).json(err);
    }
    // Avec les promesses
    // User.findById(id)
    // .then(user => res.status(200).json(user))
    // .catch(err => res.status(501).json({
    //     message: `blog post with id ${id} not found`,
    //     error: err
    // }));
}

exports.getUsers = (req,res) => {
    console.log('req : ', req);
    let filter = {};
    if(req.query.email){
        filter = {email : req.query.email};
    }
    User.find(filter)
    .exec()
    .then(user => {
        if(user && user.length > 0){
            return res.status(200).json(user);
        }
        else {
            return res.status(404).json('User not found');
        }
    })
    .catch(err => res.status(501).json({
        message: 'Server error',
        error: err
    }));
}

exports.add = async (req, res) => {
    const _id = mongoose.Types.ObjectId();
    const user = new User({ _id:_id , ...req.body});
    // user.setPassword(req.body.password);
    user.save((err)=>{
        if(err){
            return res.status(501).json(err);
        }
        return res.status(201).json(user);
    });
}

exports.update = (req, res) => {
    User.findOneAndUpdate({email: req.body.email}, req.body)
    .then(user => User.findOne({email:req.body.email})
    .then(newUser => res.status(201).json(newUser))
    .catch(err2 => res.status(501).json(err2)))
    .catch(err => res.status(501).json(err));
}

exports.delete = async (req, res, next) => {
    const id = req.params.id;

    try {
        await User.findByIdAndDelete(id);

        return res.status(201).json('delete ok');
    } catch (err) {
        return res.status(501).json(err);
    }
}