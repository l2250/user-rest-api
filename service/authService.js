const User = require('../models/user');
const mongoose = require('mongoose');
var jwt = require('jsonwebtoken');

exports.login = (req, res) => {
  console.log('req : ', req.body);
  let filter = {};
  if (req.body.email && req.body.password) {
    filter = {
      email: req.body.email,
      password: req.body.password
    };
  }
  console.log(filter)
  User.findOne(filter)
    .exec()
    .then(user => {
      if (user) {
        var token = jwt.sign({name: user.name, firstName: user.firstName}, 'supersecret', {expiresIn: 120});
        return res.status(200).json(token);
      } else {
        return res.status(404).json('User not found');
      }
    })
    .catch(err => res.status(501).json({
      message: 'Server error',
      error: err
    }));
}